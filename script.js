document.getElementById('loginForm').addEventListener('submit', function(event) {
    event.preventDefault();
    
    const username = document.getElementById('username').value;
    const password = document.getElementById('password').value;
    const errorMessage = document.getElementById('error-message');
    
    errorMessage.textContent = '';
    
    // Exemple de validation basique (à remplacer par une vérification sécurisée côté serveur)
    if (username === 'admin' && password === 'password123') {
        window.location.href = 'profile.html';
    } else {
        errorMessage.textContent = 'Nom d\'utilisateur ou mot de passe incorrect';
    }
});

if (document.getElementById('profileForm')) {
    document.getElementById('profileForm').addEventListener('submit', function(event) {
        event.preventDefault();
        
        const fullName = document.getElementById('fullName').value;
        const email = document.getElementById('email').value;
        const bio = document.getElementById('bio').value;
        const profileSuccessMessage = document.getElementById('profile-success-message');
        
        profileSuccessMessage.textContent = 'Votre profil a été mis à jour avec succès !';
        profileSuccessMessage.classList.remove('hidden');
        
        // Réinitialiser le formulaire après soumission
        document.getElementById('profileForm').reset();
    });
}
